//
//  IconPickerViewController.swift
//  Memorandum
//
//  Created by CurryZhang on 2019/10/9.
//  Copyright © 2019 CurryZhang. All rights reserved.
//

import UIKit
protocol IconPickerViewControllerDelegate: class {
    func iconPicker(_ picker: IconPickerViewController,didPick iconName: String)
}

class IconPickerViewController: UITableViewController {
    weak var delegate: IconPickerViewControllerDelegate?
    
    let icons = ["NO Icon",
                "Artboard",
                "cangshu",
                "gougou",
                "lang",
                "miao",
                "niao-",
                "panda",
                "she",
                "zhutou"
    ]
    let iconNames = ["不显示图标",
                    "猫",
                    "仓鼠",
                    "狗",
                    "狼",
                    "鸭子",
                    "鸟",
                    "熊猫",
                    "蛇",
                    "猪"
        
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

  
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return icons.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IconCell", for: indexPath)
        
        cell.textLabel!.text = iconNames[indexPath.row]
        cell.imageView!.image = UIImage(named: icons[indexPath.row])
        
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let delegate = delegate {
            let iconName = icons[indexPath.row]
            delegate.iconPicker(self, didPick: iconName)
        }
    }

}
